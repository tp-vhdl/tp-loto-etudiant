library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity compteur_valid is
    port (
        I_clk        : in  std_logic;
        I_GlobalInit : in  std_logic;
        I_comptage   : in  std_logic;
        O_adr        : out std_logic_vector(2 downto 0);
        O_fin        : out std_logic
        );
end compteur_valid;

architecture a_compteur_valid of compteur_valid is

    signal SR_Counter : integer range 0 to 5;

begin

    process (I_clk)
    begin
        if rising_edge(I_clk) then
            if I_GlobalInit = '1' then
                SR_Counter <= 0;
            elsif I_comptage = '1' then
                if SR_Counter = 5 then
                    SR_Counter <= 0;
                else
                    SR_Counter <= SR_Counter + 1;
                end if;
            end if;
        end if;
    end process;

    O_adr <= std_logic_vector(to_unsigned(SR_Counter, 3));
    O_fin <= '1' when SR_Counter = 5 else '0';

end a_compteur_valid;
