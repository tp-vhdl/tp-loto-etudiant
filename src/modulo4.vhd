library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity modulo4 is
    port (
        I_clk   : in  std_logic;
        O_Mod4  : out std_logic_vector(1 downto 0);
        O_decod : out std_logic_vector(3 downto 0)
        );

end modulo4;

architecture modulo4_a of modulo4 is

    signal SR_Counter : unsigned(1 downto 0) := (others => '0');

begin

    process (I_clk)
    begin
        if rising_edge(I_clk) then
            if SR_Counter = "11" then
                SR_Counter <= "00";
            else
                SR_Counter <= SR_Counter + 1;
            end if;
        end if;
    end process;

    O_Mod4 <= std_logic_vector(SR_Counter);

    process (SR_Counter)
    begin
        O_decod(0) <= '1';
        O_decod(1) <= '1';
        O_decod(2) <= '1';
        O_decod(3) <= '1';
        case SR_Counter is
            when "00"   => O_decod(0) <= '0';
            when "01"   => O_decod(1) <= '0';
            when "10"   => O_decod(2) <= '0';
            when others => O_decod(3) <= '0';
        end case;
    end process;

end modulo4_a;
