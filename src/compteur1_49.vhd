library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity compteur1_49 is
    port (
        I_clk      : in  std_logic;
        I_comptage : in  std_logic;
        O_sortie   : out std_logic_vector(5 downto 0)
        );

end compteur1_49;

architecture compteur_a of compteur1_49 is

    signal SR_cpt_val : unsigned(5 downto 0) := to_unsigned(1, 6);

begin

    cpt : process (I_clk)
    begin
        if rising_edge(I_clk) then
            if I_comptage = '1' then
                if SR_cpt_val = 49 then
                    SR_cpt_val <= to_unsigned(1, 6);
                else
                    SR_cpt_val <= SR_cpt_val + 1;
                end if;
            end if;
        end if;
    end process cpt;

    O_sortie <= std_logic_vector(SR_cpt_val);

end compteur_a;
