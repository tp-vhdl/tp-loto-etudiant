-------------------------------------------------------------------------------
-- Title      : Testbench for design "mux6_1"
-- Project    :
-------------------------------------------------------------------------------
-- File       : mux6_1_tb.vhd
-- Author     : Matthieu Arzel
-- Company    :
-- Created    : 2018-12-17
-- Last update: 2024-02-15
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2018
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-12-17  1.0      marzel  Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;



entity mux6_1_tb is

end entity mux6_1_tb;

architecture arch of mux6_1_tb is

    signal E0       : std_logic_vector(5 downto 0) := (0      => '1', others => '0');
    signal E1       : std_logic_vector(5 downto 0) := (1      => '1', others => '0');
    signal E2       : std_logic_vector(5 downto 0) := (2      => '1', others => '0');
    signal E3       : std_logic_vector(5 downto 0) := (3      => '1', others => '0');
    signal E4       : std_logic_vector(5 downto 0) := (4      => '1', others => '0');
    signal E5       : std_logic_vector(5 downto 0) := (5      => '1', others => '0');
    signal COMMANDE : std_logic_vector(2 downto 0) := (others => '0');
    signal S        : std_logic_vector(5 downto 0);
    signal Clk      : std_logic                    := '1';

begin
    DUT : entity work.mux6_1
        port map (
            I_0    => E0,
            I_1    => E1,
            I_2    => E2,
            I_3    => E3,
            I_4    => E4,
            I_5    => E5,
            I_sel  => COMMANDE,
            O_mux6 => S);

    Clk <= not Clk after 10 ns;

    cpt : process (clk) is
    begin
        if rising_edge(clk) then
            if(COMMANDE = "101") then
                COMMANDE <= "000";
            else
                COMMANDE <= std_logic_vector(unsigned(COMMANDE)+1);
            end if;
        end if;
    end process cpt;

end architecture arch;

-------------------------------------------------------------------------------

configuration mux6_1_tb_arch_cfg of mux6_1_tb is
    for arch
    end for;
end mux6_1_tb_arch_cfg;


-------------------------------------------------------------------------------
