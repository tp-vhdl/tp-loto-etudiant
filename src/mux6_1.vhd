library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity mux6_1 is
    port (
        I_0    : in  std_logic_vector(5 downto 0);
        I_1    : in  std_logic_vector(5 downto 0);
        I_2    : in  std_logic_vector(5 downto 0);
        I_3    : in  std_logic_vector(5 downto 0);
        I_4    : in  std_logic_vector(5 downto 0);
        I_5    : in  std_logic_vector(5 downto 0);
        I_sel  : in  std_logic_vector(2 downto 0);
        O_mux6 : out std_logic_vector(5 downto 0)
        );

end mux6_1;

-------------------------------------------------------------------------

architecture a_mux6_1 of mux6_1 is
begin

__BLANK_TO_FILL__



end a_mux6_1;
