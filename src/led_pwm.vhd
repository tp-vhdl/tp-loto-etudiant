library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity led_pwm is
    port (
        I_clk      : in  std_logic;
        I_ledR     : in  std_logic;
        I_ledV     : in  std_logic;
        O_ledR_PWM : out std_logic;
        O_ledV_PWM : out std_logic
        );
end entity led_pwm;

architecture arch of led_pwm is

    signal SR_cpt_leds     : unsigned(4 downto 0) := (others => '0');
    signal SR_cpt_leds_reg : unsigned(4 downto 0) := (others => '0');

begin

    leds_pwm : process (I_clk) is
    begin
        if rising_edge(I_clk) then
            SR_cpt_leds     <= SR_cpt_leds + 1;
            SR_cpt_leds_reg <= SR_cpt_leds;
        end if;
    end process leds_pwm;

    O_ledR_PWM <= I_ledR and SR_cpt_leds(4) and not(SR_cpt_leds_reg(4));
    O_ledV_PWM <= I_ledV and SR_cpt_leds(4) and not(SR_cpt_leds_reg(4));

end architecture arch;
