--------------------------------------------------------------
-- Title      : Testbench for design "loto"
-- Project    :
-------------------------------------------------------------------------------
-- File       : loto_tb.vhd
-- Author     : Matthieu Arzel  <mattieu.arzel@imt-atlantique.fr>
-- Company    :
-- Created    : 2018-06-14
-- Last update: 2024-03-26
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2018
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-06-14  1.0      marzel  Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity loto_tb is

end entity loto_tb;

-------------------------------------------------------------------------------

architecture ar of loto_tb is

    -- component generics
    constant n_fast : natural := 0;
    constant n_slow : natural := 3;

    -- component ports
    signal bouton, bloque, clk_100m, rst : std_logic := '0';
    signal s_aff                         : std_logic_vector(6 downto 0);
    signal rouge, vert                   : std_logic;
    signal an                            : std_logic_vector(7 downto 0);



begin  -- architecture ar

    -- component instantiation
    DUT : entity work.loto
        generic map (
            n_fast => n_fast,
            n_slow => n_slow)
        port map (
            I_clk_100m        => clk_100m,
            I_GlobalInit      => rst,
            I_button          => bouton,
            I_block           => bloque,
            O_7segmentDisplay => s_aff,
            O_7segmentSelect  => an,
            O_red             => rouge,
            O_green           => vert
            );

    -- clock generation
    clk_100m <= not clk_100m after 10 ns;

    bloque <= '0';
    rst    <= '0', '1' after 2 ns, '0' after 14 ns;

    bouton <= '0', '1' after 37 ns, '0' after 137 ns, '1' after 184 ns,
              '0' after 259 ns, '1' after 312 ns, '0' after 496 ns,
              '1' after 532 ns, '0' after 876 ns, '1' after 935 ns,
              '0' after 1023 ns, '1' after 1232 ns, '0' after 1543 ns,
              '1' after 1670 ns, '0' after 4523 ns, '1' after 6634 ns,
              '0' after 7872 ns, '1' after 8212 ns, '0' after 8398 ns;



end architecture ar;

-------------------------------------------------------------------------------

configuration loto_tb_ar_cfg of loto_tb is
    for ar
    end for;
end loto_tb_ar_cfg;

-------------------------------------------------------------------------------
