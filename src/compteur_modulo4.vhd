library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity compteur_modulo4 is
    port (
        I_clk         : in  std_logic;
        O_CounterMod4 : out std_logic_vector(1 downto 0);
        O_parallel1   : out std_logic;
        O_parallel2   : out std_logic;
        O_parallel3   : out std_logic;
        O_parallel4   : out std_logic
        );
end compteur_modulo4;

architecture modulo4_a of compteur_modulo4 is

    signal SR_Counter : unsigned(1 downto 0) := (others => '0');

begin

    mod4 : process (clk)

    begin
        if rising_edge(clk) then
            if SR_Counter = "11" then
                SR_Counter <= "00";
            else
                SR_Counter <= SR_Counter + 1;
            end if;
        end if;
    end process mod4;

    O_CounterMod4 <= std_logic_vector(SR_Counter);

    parallel : process (SR_Counter)
    begin
        case SR_Counter is
            when "00" =>
                O_parallel1 <= '0';
                O_parallel2 <= '1';
                O_parallel3 <= '1';
                O_parallel4 <= '1';

            when "01" =>
                O_parallel1 <= '1';
                O_parallel2 <= '0';
                O_parallel3 <= '1';
                O_parallel4 <= '1';

            when "10" =>
                O_parallel1 <= '1';
                O_parallel2 <= '1';
                O_parallel3 <= '0';
                O_parallel4 <= '1';

            when others =>
                O_parallel1 <= '1';
                O_parallel2 <= '1';
                O_parallel3 <= '1';
                O_parallel4 <= '0';

        end case;
    end process parallel;

end modulo4_a;
