library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity compteur_modulo6 is
    port (
        I_clk         : in  std_logic;
        I_block       : in  std_logic;
        O_CounterMod6 : out std_logic_vector(2 downto 0)
        );
end compteur_modulo6;

architecture modulo6_a of compteur_modulo6 is

    signal SR_Counter : unsigned(2 downto 0) := (others => '0');

begin

    process (_BLANK_)
    begin
        if rising_edge(I_clk) then
            _BLANK_
        end if;
    end process;

    O_CounterMod6 <= std_logic_vector(SR_Counter);

end modulo6_a;
