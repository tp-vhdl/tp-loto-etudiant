library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity loto is
    generic (
        n_fast : natural := 15;
        n_slow : natural := 25);
    port (
        I_button          : in  std_logic;
        I_block           : in  std_logic;
        I_clk_100m        : in  std_logic;
        I_GlobalInit      : in  std_logic;
        O_7segmentDisplay : out std_logic_vector(6 downto 0);
        O_7segmentSelect  : out std_logic_vector(7 downto 0);
        O_red             : out std_logic;
        O_green           : out std_logic
        );
end entity loto;

architecture arch of loto is

    component tirage is
        port (
            I_clk         : in  std_logic;
            I_GlobalInit  : in  std_logic;
            I_clk_display : in  std_logic;
            I_button      : in  std_logic;
            O_reg0        : out std_logic_vector(5 downto 0);
            O_reg1        : out std_logic_vector(5 downto 0);
            O_reg2        : out std_logic_vector(5 downto 0);
            O_reg3        : out std_logic_vector(5 downto 0);
            O_reg4        : out std_logic_vector(5 downto 0);
            O_reg5        : out std_logic_vector(5 downto 0);
            O_l_red       : out std_logic;
            O_l_green     : out std_logic);
    end component tirage;

    component compteur_modulo6 is
        port (
            I_clk         : in  std_logic;
            I_block       : in  std_logic;
            O_CounterMod6 : out std_logic_vector(2 downto 0));
    end component compteur_modulo6;

    component diviseur_freq is
        generic (
            n_fast : natural;
            n_slow : natural);
        port (
            I_clk  : in  std_logic;
            O_fast : out std_logic;
            O_slow : out std_logic
            );
    end component diviseur_freq;

    component transcodeur7s_d_u is
        port (
            I_number : in  std_logic_vector(5 downto 0);
            O_unit   : out std_logic_vector (6 downto 0);
            O_ten    : out std_logic_vector (6 downto 0)
            );
    end component transcodeur7s_d_u;

    component mux6_1 is
        port (
            I_0    : in  std_logic_vector(5 downto 0);
            I_1    : in  std_logic_vector(5 downto 0);
            I_2    : in  std_logic_vector(5 downto 0);
            I_3    : in  std_logic_vector(5 downto 0);
            I_4    : in  std_logic_vector(5 downto 0);
            I_5    : in  std_logic_vector(5 downto 0);
            I_sel  : in  std_logic_vector(2 downto 0);
            O_mux6 : out std_logic_vector(5 downto 0)
            );
    end component mux6_1;

    component modulo4 is
        port (
            I_clk   : in  std_logic;
            O_Mod4  : out std_logic_vector(1 downto 0);
            O_decod : out std_logic_vector(3 downto 0)
            );
    end component modulo4;

    signal SC_clkDisplay     : std_logic;
    signal SC_clk            : std_logic;
    signal SC_reg0           : std_logic_vector(5 downto 0);
    signal SC_reg1           : std_logic_vector(5 downto 0);
    signal SC_reg2           : std_logic_vector(5 downto 0);
    signal SC_reg3           : std_logic_vector(5 downto 0);
    signal SC_reg4           : std_logic_vector(5 downto 0);
    signal SC_reg5           : std_logic_vector(5 downto 0);
    signal SC_regReadAddress : std_logic_vector(2 downto 0);
    signal SC_selectedReg    : std_logic_vector(5 downto 0);
    signal SC_unit           : std_logic_vector(6 downto 0);
    signal SC_ten            : std_logic_vector(6 downto 0);
    signal SC_minus          : std_logic_vector(6 downto 0);
    signal SC_numTranscode   : std_logic_vector(6 downto 0);
    signal SC_displaySelect  : std_logic_vector(1 downto 0);

begin

    tirage_1 : entity work.tirage
        port map (
            I_clk         => SC_clk,
            I_GlobalInit  => I_GlobalInit,
            I_clk_display => SC_clkDisplay,
            I_button      => I_button,
            O_reg0        => SC_reg0,
            O_reg1        => SC_reg1,
            O_reg2        => SC_reg2,
            O_reg3        => SC_reg3,
            O_reg4        => SC_reg4,
            O_reg5        => SC_reg5,
            O_l_red       => O_red,
            O_l_green     => O_green
            );

    modulo6_1 : entity work.compteur_modulo6
        port map (
            I_clk         => SC_clkDisplay,
            I_block       => I_block,
            O_CounterMod6 => SC_regReadAddress
            );

    diviseur_freq_1 : entity work.diviseur_freq
        generic map (
            n_fast => n_fast,
            n_slow => n_slow)
        port map (
            I_clk  => I_clk_100m,
            O_fast => SC_clk,
            O_slow => SC_clkDisplay
            );

    mux6_1_1 : entity work.mux6_1
        port map (
            I_0    => SC_reg0,
            I_1    => SC_reg1,
            I_2    => SC_reg2,
            I_3    => SC_reg3,
            I_4    => SC_reg4,
            I_5    => SC_reg5,
            I_sel  => SC_regReadAddress,
            O_mux6 => SC_selectedReg
            );

    transcod_1 : entity work.transcodeur7s_d_u(transcod_int)  --(transcod_ARCH)
        port map (
            I_number => SC_selectedReg,
            O_unit   => SC_unit,
            O_ten    => SC_ten
            );

    modulo4_2 : entity work.modulo4
        port map (
            I_clk   => SC_clk,
            O_Mod4  => SC_displaySelect,
            O_decod => O_7segmentSelect(3 downto 0)
            );


    with to_integer(unsigned(SC_regReadAddress)) select SC_numTranscode <=
        "1111001" when 0,               -- 1 for a 0 for better readability
        "0100100" when 1,
        "0110000" when 2,
        "0011001" when 3,
        "0010010" when 4,
        "0000010" when 5,
        "0110000" when others;

    O_7segmentSelect(4) <= '1';
    O_7segmentSelect(5) <= '1';
    O_7segmentSelect(6) <= '1';
    O_7segmentSelect(7) <= '1';

    SC_minus <= "0111111";

    with SC_displaySelect select O_7segmentDisplay <=
        SC_unit         when "00",
        SC_ten          when "01",
        SC_minus        when "10",
        SC_numTranscode when others;

end architecture arch;
