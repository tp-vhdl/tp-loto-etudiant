library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity registres is

    port (
        I_clk        : in  std_logic;
        I_GlobalInit : in  std_logic;
        I_wr         : in  std_logic;
        I_adr        : in  std_logic_vector(2 downto 0);
        I_data       : in  std_logic_vector(5 downto 0);
        O_reg0       : out std_logic_vector(5 downto 0);
        O_reg1       : out std_logic_vector(5 downto 0);
        O_reg2       : out std_logic_vector(5 downto 0);
        O_reg3       : out std_logic_vector(5 downto 0);
        O_reg4       : out std_logic_vector(5 downto 0);
        O_reg5       : out std_logic_vector(5 downto 0)
        );

end registres;

architecture a_registres of registres is

begin

    process (I_clk)
    begin

        if rising_edge(I_clk) then
            if I_GlobalInit = '1' then
                O_reg0 <= (others => '0');
                O_reg1 <= (others => '0');
                O_reg2 <= (others => '0');
                O_reg3 <= (others => '0');
                O_reg4 <= (others => '0');
                O_reg5 <= (others => '0');
            elsif I_wr = '1' then
              case I_adr is
                    when "000"  => O_reg0 <= I_data;
                    when "001"  => O_reg1 <= I_data;
                    when "010"  => O_reg2 <= I_data;
                    when "011"  => O_reg3 <= I_data;
                    when "100"  => O_reg4 <= I_data;
                    when "101"  => O_reg5 <= I_data;
                    when others => null;
                end case;
            end if;
        end if;
    end process;

end a_registres;
