library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity diviseur_freq is
    generic (
        n_fast : natural := 15;
        n_slow : natural := 24
        );
    port (
        I_clk  : in  std_logic;
        O_fast : out std_logic;
        O_slow : out std_logic
        );
end diviseur_freq;

architecture Behavioral of diviseur_freq is

    signal SR_counter : unsigned (26 downto 0) := (others => '0');

begin

    process (I_clk)
    begin
        if rising_edge(I_clk) then
            SR_counter <= SR_counter + 1;
        end if;
    end process;

    O_fast <= SR_counter(n_fast);
    O_slow <= SR_counter(n_slow);

end Behavioral;
