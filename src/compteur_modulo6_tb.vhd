-------------------------------------------------------------------------------
-- Title      : Testbench for design "compteur_modulo6"
-- Project    :
-------------------------------------------------------------------------------
-- File       : compteur_modulo6_tb.vhd
-- Author     : Matthieu Arzel  <marzel@marzel-XPS-13-9350>
-- Company    :
-- Created    : 2019-01-10
-- Last update: 2024-03-26
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2019
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2019-01-10  1.0      marzel  Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity compteur_modulo6_tb is
end entity compteur_modulo6_tb;

architecture arch of compteur_modulo6_tb is

    signal SC_block       : std_logic := '0';
    signal SC_CounterMod6 : std_logic_vector(2 downto 0);
    signal SR_Clk         : std_logic := '1';

begin

    DUT : entity work.compteur_modulo6
        port map (
            I_clk         => SR_Clk,
            I_block       => SC_block,
            O_counterMod6 => SC_CounterMod6
            );

    SR_Clk   <= not SR_Clk after 7 ns;
    SC_block <= '0', '1'   after 33 ns, '0' after 79 ns, '1' after 211 ns, '0' after 251 ns;

end architecture arch;

-------------------------------------------------------------------------------

configuration compteur_modulo6_tb_arch_cfg of compteur_modulo6_tb is
    for arch
    end for;
end compteur_modulo6_tb_arch_cfg;

-------------------------------------------------------------------------------
