library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

entity transcodeur7s_d_u is
    port (
        I_number : in  std_logic_vector(5 downto 0);
        O_unit    : out std_logic_vector (6 downto 0);
        O_ten    : out std_logic_vector (6 downto 0)
        );
end transcodeur7s_d_u;

architecture transcod_ARCH of transcodeur7s_d_u is

    signal SC_u_nombre : unsigned(5 downto 0);
    signal SC_u_unit    : unsigned(5 downto 0);
    signal SC_u_ten    : unsigned(2 downto 0);

begin

    SC_u_nombre <= unsigned(I_number);

    p_diz : process (SC_u_nombre) is
    begin
        if (SC_u_nombre < 10) then
            SC_u_ten <= (others => '0');
            SC_u_unit <= SC_u_nombre;
        elsif SC_u_nombre < 20 then
            SC_u_ten <= to_unsigned(1, 3);
            SC_u_unit <= SC_u_nombre - 10;
        elsif SC_u_nombre < 30 then
            SC_u_ten <= to_unsigned(2, 3);
            SC_u_unit <= SC_u_nombre - 20;
        elsif SC_u_nombre < 40 then
            SC_u_ten <= to_unsigned(3, 3);
            SC_u_unit <= SC_u_nombre - 30;
        else
            SC_u_ten <= to_unsigned(4, 3);
            SC_u_unit <= SC_u_nombre - 40;
        end if;
    end process p_diz;

    with to_integer(SC_u_unit) select O_unit <=
        "1000000" when 0,
        "1111001" when 1,
        "0100100" when 2,
        "0110000" when 3,
        "0011001" when 4,
        "0010010" when 5,
        "0000010" when 6,
        "1111000" when 7,
        "0000000" when 8,
        "0010000" when others;

    with to_integer(SC_u_ten) select O_ten <=
        "1000000" when 0,
        "1111001" when 1,
        "0100100" when 2,
        "0110000" when 3,
        "0011001" when others;

end transcod_ARCH;

architecture transcod_int of transcodeur7s_d_u is

    signal SC_unit_int : integer range 0 to 9;
    signal SC_diz_int : integer range 0 to 4;
    signal SC_num_int : integer range 0 to 49;

begin

    SC_num_int <= to_integer(unsigned(I_number));
    SC_unit_int <= SC_num_int rem 10;
    SC_diz_int <= SC_num_int / 10;

    with SC_unit_int select O_unit <=
        "1000000" when 0,
        "1111001" when 1,
        "0100100" when 2,
        "0110000" when 3,
        "0011001" when 4,
        "0010010" when 5,
        "0000010" when 6,
        "1111000" when 7,
        "0000000" when 8,
        "0010000" when others;

    with SC_diz_int select O_ten <=
        "1000000" when 0,
        "1111001" when 1,
        "0100100" when 2,
        "0110000" when 3,
        "0011001" when others;

end transcod_int;
