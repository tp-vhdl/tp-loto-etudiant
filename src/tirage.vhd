library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity tirage is

    port (
        I_clk         : in  std_logic;
        I_GlobalInit  : in  std_logic;
        I_clk_display : in  std_logic;
        I_button      : in  std_logic;
        O_reg0        : out std_logic_vector(5 downto 0);
        O_reg1        : out std_logic_vector(5 downto 0);
        O_reg2        : out std_logic_vector(5 downto 0);
        O_reg3        : out std_logic_vector(5 downto 0);
        O_reg4        : out std_logic_vector(5 downto 0);
        O_reg5        : out std_logic_vector(5 downto 0);
        O_l_red       : out std_logic;
        O_l_green     : out std_logic
        );

end tirage;

architecture a_tirage of tirage is

    component automate is
        port (
            I_clk            : in  std_logic;
            I_GlobalInit     : in  std_logic;
            I_clk_display    : in  std_logic;
            I_button         : in  std_logic;
            I_invalide       : in  std_logic;
            I_fin            : in  std_logic;
            O_comptage       : out std_logic;
            O_enregistrement : out std_logic;
            O_l_red          : out std_logic;
            O_l_green        : out std_logic);
    end component automate;

    component registres is
        port (
            I_clk        : in  std_logic;
            I_GlobalInit : in  std_logic;
            I_wr         : in  std_logic;
            I_adr        : in  std_logic_vector(2 downto 0);
            I_data       : in  std_logic_vector(5 downto 0);
            O_reg0       : out std_logic_vector(5 downto 0);
            O_reg1       : out std_logic_vector(5 downto 0);
            O_reg2       : out std_logic_vector(5 downto 0);
            O_reg3       : out std_logic_vector(5 downto 0);
            O_reg4       : out std_logic_vector(5 downto 0);
            O_reg5       : out std_logic_vector(5 downto 0));
    end component registres;

    component compteur_valid is
        port (
            I_clk        : in  std_logic;
            I_GlobalInit : in  std_logic;
            I_comptage   : in  std_logic;
            O_adr        : out std_logic_vector(2 downto 0);
            O_fin        : out std_logic);
    end component compteur_valid;

    component comparateur is
        port (
            I_reg0           : in  std_logic_vector(5 downto 0);
            I_reg1           : in  std_logic_vector(5 downto 0);
            I_reg2           : in  std_logic_vector(5 downto 0);
            I_reg3           : in  std_logic_vector(5 downto 0);
            I_reg4           : in  std_logic_vector(5 downto 0);
            I_nombre_courant : in  std_logic_vector(5 downto 0);
            O_invalide       : out std_logic);
    end component comparateur;

    component compteur1_49 is
        port (
            I_clk      : in  std_logic;
            I_comptage : in  std_logic;
            O_sortie   : out std_logic_vector(5 downto 0));
    end component compteur1_49;

    component led_pwm is
        port (
            I_clk      : in  std_logic;
            I_ledR     : in  std_logic;
            I_ledV     : in  std_logic;
            O_ledR_PWM : out std_logic;
            O_ledV_PWM : out std_logic
            );
    end component led_pwm;

    signal SC_recom          : std_logic;
    signal SC_comptage       : std_logic;
    signal SC_enregistrement : std_logic;
    signal SC_invalide       : std_logic;
    signal SC_fin            : std_logic;
    signal SC_adr            : std_logic_vector(2 downto 0);
    signal SC_numero_courant : std_logic_vector(5 downto 0);
    signal SC_r0             : std_logic_vector(5 downto 0);
    signal SC_r1             : std_logic_vector(5 downto 0);
    signal SC_r2             : std_logic_vector(5 downto 0);
    signal SC_r3             : std_logic_vector(5 downto 0);
    signal SC_r4             : std_logic_vector(5 downto 0);
    signal SC_r5             : std_logic_vector(5 downto 0);
    signal SC_l_V            : std_logic;
    signal SC_l_R            : std_logic;
    signal SC_cpt_leds       : unsigned(4 downto 0);
    signal SC_cpt_leds_reg   : unsigned(4 downto 0);

begin

    automate_1 : entity work.automate
        port map (
            I_GlobalInit  => I_GlobalInit,
            I_clk         => I_clk,
            I_clk_display => I_clk_display,
            I_button      => I_button,
            I_invalide    => SC_invalide,
            I_end         => SC_fin,
            O_counting    => SC_comptage,
            O_store       => SC_enregistrement,
            O_l_red       => SC_l_R,
            O_l_green     => SC_l_V
            );

    registres_2 : entity work.registres
        port map (
            I_clk        => I_clk,
            I_GlobalInit => I_GlobalInit,
            I_wr         => SC_enregistrement,
            I_adr        => SC_adr,
            I_data       => SC_numero_courant,
            O_reg0       => SC_r0,
            O_reg1       => SC_r1,
            O_reg2       => SC_r2,
            O_reg3       => SC_r3,
            O_reg4       => SC_r4,
            O_reg5       => SC_r5
            );

    compteur_valid_1 : entity work.compteur_valid
        port map (
            I_clk        => I_clk,
            I_GlobalInit => I_GlobalInit,
            I_comptage   => SC_enregistrement,
            O_adr        => SC_adr,
            O_fin        => SC_fin
            );

    compteur_1 : entity work.compteur1_49
        port map (
            I_clk      => I_clk,
            I_comptage => SC_comptage,
            O_sortie   => SC_numero_courant
            );

    led_pwm_1 : entity work.led_pwm
        port map (
            I_clk      => I_clk,
            I_ledR     => SC_l_R,
            I_ledV     => SC_l_V,
            O_ledR_PWM => O_l_red,
            O_ledV_PWM => O_l_green
            );

    SC_invalide <= '1' when (SC_r0 = SC_numero_courant or
                             SC_r1 = SC_numero_courant or
                             SC_r2 = SC_numero_courant or
                             SC_r3 = SC_numero_courant or
                             SC_r4 = SC_numero_courant
                             ) else '0';

    O_reg0 <= SC_r0;
    O_reg1 <= SC_r1;
    O_reg2 <= SC_r2;
    O_reg3 <= SC_r3;
    O_reg4 <= SC_r4;
    O_reg5 <= SC_r5;

end a_tirage;
