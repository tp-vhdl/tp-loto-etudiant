##Bank = 15, Pin name = IO_L11N_T1_SRCC_15,					Sch name = BTNC
set_property PACKAGE_PIN E16 [get_ports I_button]
set_property IOSTANDARD LVCMOS33 [get_ports I_button]

##Bank = 34, Pin name = IO_L14P_T2_SRCC_34,					Sch name = SW15
set_property PACKAGE_PIN P4 [get_ports I_block]
set_property IOSTANDARD LVCMOS33 [get_ports I_block]

## Clock signal
##Bank = 35, Pin name = IO_L12P_T1_MRCC_35,					Sch name = CLK100MHZ
set_property PACKAGE_PIN E3 [get_ports I_clk_100m]
set_property IOSTANDARD LVCMOS33 [get_ports I_clk_100m]
create_clock -add -name sys_clk_pin -period 10.00 -waveform {0 5} [get_ports I_clk_100m]

##Bank = 14, Pin name = IO_L21P_T3_DQS_14,					Sch name = BTND
set_property PACKAGE_PIN V10 [get_ports I_GlobalInit]
set_property IOSTANDARD LVCMOS33 [get_ports I_GlobalInit]


 ##Bank = 34, Pin name = IO_L5P_T0_34,						Sch name = LED16_R
set_property PACKAGE_PIN K5 [get_ports O_red]
set_property IOSTANDARD LVCMOS33 [get_ports O_red]

##Bank = 35, Pin name = IO_24P_T3_35,						Sch name =  LED17_G
set_property PACKAGE_PIN H6 [get_ports O_green]
set_property IOSTANDARD LVCMOS33 [get_ports O_green]

#7 O_affment display
#Bank = 34, Pin name = IO_L2N_T0_34,						Sch name = CA
set_property PACKAGE_PIN L3 [get_ports {O_7segmentDisplay[0]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentDisplay[0]}]
#Bank = 34, Pin name = IO_L3N_T0_DQS_34,					Sch name = CB
set_property PACKAGE_PIN N1 [get_ports {O_7segmentDisplay[1]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentDisplay[1]}]
#Bank = 34, Pin name = IO_L6N_T0_VREF_34,					Sch name = CC
set_property PACKAGE_PIN L5 [get_ports {O_7segmentDisplay[2]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentDisplay[2]}]
#Bank = 34, Pin name = IO_L5N_T0_34,						Sch name = CD
set_property PACKAGE_PIN L4 [get_ports {O_7segmentDisplay[3]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentDisplay[3]}]
#Bank = 34, Pin name = IO_L2P_T0_34,						Sch name = CE
set_property PACKAGE_PIN K3 [get_ports {O_7segmentDisplay[4]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentDisplay[4]}]
#Bank = 34, Pin name = IO_L4N_T0_34,						Sch name = CF
set_property PACKAGE_PIN M2 [get_ports {O_7segmentDisplay[5]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentDisplay[5]}]
#Bank = 34, Pin name = IO_L6P_T0_34,						Sch name = CG
set_property PACKAGE_PIN L6 [get_ports {O_7segmentDisplay[6]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentDisplay[6]}]

#Bank = 34, Pin name = IO_L18N_T2_34,						Sch name = AN0
set_property PACKAGE_PIN N6 [get_ports {O_7segmentSelect[0]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[0]}]
#Bank = 34, Pin name = IO_L18P_T2_34,						Sch name = AN1
set_property PACKAGE_PIN M6 [get_ports {O_7segmentSelect[1]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[1]}]
#Bank = 34, Pin name = IO_L4P_T0_34,						Sch name = AN2
set_property PACKAGE_PIN M3 [get_ports {O_7segmentSelect[2]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[2]}]
#Bank = 34, Pin name = IO_L13_T2_MRCC_34,					Sch name = AN3
set_property PACKAGE_PIN N5 [get_ports {O_7segmentSelect[3]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[3]}]
#Bank = 34, Pin name = IO_L3P_T0_DQS_34,					Sch name = AN4
set_property PACKAGE_PIN N2 [get_ports {O_7segmentSelect[4]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[4]}]
#Bank = 34, Pin name = IO_L16N_T2_34,						Sch name = AN5
set_property PACKAGE_PIN N4 [get_ports {O_7segmentSelect[5]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[5]}]
#Bank = 34, Pin name = IO_L1P_T0_34,						Sch name = AN6
set_property PACKAGE_PIN L1 [get_ports {O_7segmentSelect[6]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[6]}]
#Bank = 34, Pin name = IO_L1N_T034,						Sch name = AN7
set_property PACKAGE_PIN M1 [get_ports {O_7segmentSelect[7]}]
	set_property IOSTANDARD LVCMOS33 [get_ports {O_7segmentSelect[7]}]
