-------------------------------------------------------------------------------
-- Title      :
-- Project    :
-------------------------------------------------------------------------------
-- File       : transcodeur7s_u.vhd
-- Author     : Jean-Noel BAZIN  <jnbazin@pc-disi-026.enst-bretagne.fr>
-- Company    :
-- Created    : 2023-10-12
-- Last update: 2023-10-12
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: converts the 3-bit representation of a decimal (I_nombre) into a 7-segment display command (O_uni).
-- A display command is active low:  to turn on a segment, apply 0.
-- a (= O_uni(6)) is the MSB, g (=O_uni(0)) is the LSB.
-- So, for digit 3 the output will be represented by "0000110".
--      _______
--     |   a   |
--    f|       |b
--     |       |
--     |_______|
--     |   g   |
--    e|       |c
--     |       |
--     |_______|
--         d
--
-- For this 7 segment transcoder, only the number between [0-5] are taken into account. In addition, there is a shift by 1. 0 will be represented by a 1 on the 7 segment display.
--
-------------------------------------------------------------------------------
-- Copyright (c) 2023
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2018-05-30  1.0      jnbazin Created
-------------------------------------------------------------------------------



library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity transcodeur7s_u is
    port (
        I_nombre : in  std_logic_vector(2 downto 0);
        O_uni    : out std_logic_vector(6 downto 0)
        );
end transcodeur7s_u;

architecture Behavioral of transcodeur7s_u is

begin

    with to_integer(I_nombre) select O_uni <=
        "1111001" when 0,  -- 1 for a 0
        "0100100" when 1,
        "0110000" when 2,
        "0011001" when 3,
        "0010010" when 4,
        "0000010" when 5,
        "0110000" when others;

end Behavioral;
