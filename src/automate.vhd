library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity automate is
    port (
        I_clk         : in  std_logic;
        I_GlobalInit  : in  std_logic;
        I_clk_display : in  std_logic;
        I_button      : in  std_logic;
        I_invalide    : in  std_logic;
        I_end         : in  std_logic;
        O_counting    : out std_logic;
        O_store       : out std_logic;
        O_l_red       : out std_logic;
        O_l_green     : out std_logic
        );
end automate;

architecture a_automate of automate is

    type TYPE_ETAT is (
        st_wait_failed,
        st_wait_success,
        st_counting,
        st_compar,
        st_store,
        st_end_green,
        st_end_red
        );
    signal SR_STATE : TYPE_ETAT := st_wait_success;

begin

    process (I_clk)
    begin
        if rising_edge(I_clk)then
            if I_GlobalInit = '1' then
                SR_STATE <= st_wait_success;
            else

                case SR_STATE is

                    when st_wait_success =>
                        if I_button = '1' then
                            SR_STATE <= st_counting;
                        end if;

                        when __BLANK_TO_FILL__

                            __BLANK_TO_FILL__

                end case;
            end if;
        end if;
    end process;

    O_counting <= '1' when(__BLANk_TO_FILL__
    O_store    <= '1' when(__BLANk_TO_FILL__
    O_l_red    <= '1' when(__BLANk_TO_FILL__
    O_l_green  <= '1' when(__BLANk_TO_FILL__


end a_automate;
