# TP Loto

Énoncé du TP : [https://tp-vhdl.gitlab-pages.imt-atlantique.fr/loto/](https://tp-vhdl.gitlab-pages.imt-atlantique.fr/loto/)

## Question Loto 1 : Quels sont les signaux à renseigner dans la liste de sensibilité (si vous utilisez un process explicite) ?


## Question Loto 2 : Que se passe-t-il si le test est incomplet, c’est-à-dire s’il ne couvre pas toutes les combinaisons d’entrées du module ? Est-ce grave ?


## Question Loto 3 : Ce test est-il concluant ? Est-il suffisant pour valider le module ? Justifiez.


## Question Loto 4 : Quel(s) signal(aux) doit on renseigner dans la liste de sensibilité de ce processus séquentiel ? Pourquoi ?


## Question Loto 5 : Que se passe-t-il si le test est incomplet, c’est-à-dire s’il ne couvre pas toutes les combinaisons d’entrées du module ? Est-ce grave ici ?


## Question Loto 6 : Ce test est-il concluant ? Est-il suffisant pour valider le module ? Justifiez.


## Question Loto 7 : Combien de processus avez-vous décris ?


## Question Loto 8 : De quel(s) type(s) sont-ils


## Question Loto 9 : Serait-il possible de décrire cette machine d'état de manière différente, en terme de nombre et de type de process ?


## Question Loto 10 : Ce test est-il concluant ? Justifiez.


## Question Loto 11 : Le circuit inféré par l’outil est-il conforme à l’attendu ? Sinon, en quoi diffère-t-il et est-ce lié à une erreur de description VHDL ?


## Question Loto 12 : Quelles sont les ressources utilisées sur le FPGA ? En quelle quantité/proportion des ressources disponibles ? Des **LATCHES** sont-ils utilisés ? Est-ce positif ou pas, pourquoi ?


## Question Loto 13 : Le tirage est-il aléatoire pour un humain ? pour une machine ? Justifiez.
